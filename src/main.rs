#![no_main]
#![no_std]

extern crate panic_semihosting;
use core::cell::RefCell;
use cortex_m::{interrupt::Mutex, peripheral::syst::SystClkSource::Core, Peripherals};
use cortex_m_rt::{entry, exception};
use stm32_usbd::UsbBus;
use stm32f0xx_hal::{adc, delay::Delay, prelude::*, stm32};
use usb_device::prelude::*;
use usbd_serial::{SerialPort, USB_CLASS_CDC};
fn nibble2hex(byte: u8) -> u8 {
    if byte >= 10 {
        byte - 10 + 'A' as u8
    } else {
        byte + '0' as u8
    }
}

enum Action {
    ReadBattery,
    BlinkLed,
}

static ACTION: Mutex<RefCell<Option<Action>>> = Mutex::new(RefCell::new(None));
static TICK: Mutex<RefCell<u32>> = Mutex::new(RefCell::new(1));

#[entry]
fn main() -> ! {
    let mut dp = stm32::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    // TSSOP20 need remap from PA9/PA10 to PA11/PA12
    dp.RCC.apb2enr.modify(|_, w| w.syscfgen().set_bit());
    dp.SYSCFG.cfgr1.modify(|_, w| w.pa11_pa12_rmp().remapped());

    let mut rcc = dp
        .RCC
        .configure()
        .hsi48()
        .enable_crs(dp.CRS)
        .sysclk(48.mhz())
        .pclk(24.mhz())
        .freeze(&mut dp.FLASH);

    let mut syst = cp.SYST;
    unsafe { syst.cvr.write(1) };
    syst.set_clock_source(Core);
    // Set reload value, i.e. timer delay 48 MHz/48_000 counts == 100kHz or 1ms
    syst.set_reload(48_000 - 1);
    syst.enable_counter();
    syst.enable_interrupt();
    let gpiob = dp.GPIOB.split(&mut rcc);
    let mut led = cortex_m::interrupt::free(|cs| gpiob.pb1.into_push_pull_output(cs));
    led.set_low().ok();

    let gpioa = dp.GPIOA.split(&mut rcc);

    let (mut bat_in, usb_dm, usb_dp, mut fet_5v, mut fet_12v) = cortex_m::interrupt::free(|cs| {
        (
            gpioa.pa0.into_analog(cs),
            gpioa.pa11,
            gpioa.pa12,
            gpioa.pa4.into_push_pull_output(cs),
            gpioa.pa5.into_push_pull_output(cs),
        )
    });
    fet_12v.set_low().ok();
    fet_5v.set_low().ok();

    let mut adc = adc::Adc::new(dp.ADC, &mut rcc);
    let usb_bus = UsbBus::new(dp.USB, (usb_dm, usb_dp));

    let mut serial = SerialPort::new(&usb_bus);

    let mut usb_dev = UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
        .manufacturer("SevenB4")
        .product("Serial port")
        .serial_number("1")
        .device_class(USB_CLASS_CDC)
        .build();

    // let mut delay = Delay::new(cp.SYST, &mut rcc);
    let mut blink = 0;
    let mut batvalue: u16 = 0;
    let mut toggle_led = true;
    let mut toggle_tick = false;
    loop {
        cortex_m::interrupt::free(|cs| {
            blink = *TICK.borrow(cs).borrow();
        });
        if usb_dev.poll(&mut [&mut serial]) {
            let mut buf = [0u8; 64];
            match serial.read(&mut buf) {
                Ok(count) if count > 0 => match buf[0] as char {
                    'a' => {
                        fet_5v.set_low().ok();
                        serial.write(b"OK\n").ok();
                    }
                    'A' => {
                        fet_5v.set_high().ok();
                        serial.write(b"OK\n").ok();
                    }
                    'b' => {
                        fet_12v.set_low().ok();
                        serial.write(b"OK\n").ok();
                    }
                    'B' => {
                        fet_12v.set_high().ok();
                        serial.write(b"OK\n").ok();
                    }
                    '?' | 'h' | 'H' => {
                        serial
                            .write(b"There is no help read the code your lazy neerd\n")
                            .ok();
                    }
                    'L' => {
                        toggle_led = true;
                        serial.write(b"OK\n").ok();
                    }
                    'l' => {
                        toggle_led = false;
                        serial.write(b"OK\n").ok();
                    }
                    't' => toggle_tick = false,
                    'T' => toggle_tick = true,
                    's' | 'S' => {
                        buf[2] = nibble2hex((batvalue & 0x000F) as u8);
                        buf[1] = nibble2hex(((batvalue & 0x00F0) >> 4) as u8);
                        buf[0] = nibble2hex(((batvalue & 0x0F00) >> 8) as u8);
                        buf[3] = '\n' as u8;
                        serial.write(&buf[0..4]).ok();
                    }
                    _ => {}
                },
                _ => {}
            }
        }
        if (blink % 5000) == 0 {
            led.set_low().ok();
            batvalue = if let Ok(val) = adc.read(&mut bat_in) {
                val
            } else {
                batvalue
            }
        }

        if toggle_tick && (blink % 1000) == 0 {
            serial.write(&[b'.' as u8]).ok();
        }
        if toggle_led {
            if (blink % 1000) == 0 {
                led.set_high().ok();
            } else if (blink % 100) == 0 {
                led.set_low().ok();
            }
        }
        cortex_m::asm::wfi();
        //delay.delay_ms(1_u16);
    }
}

#[exception]
#[allow(non_snake_case)]
fn SysTick() {
    cortex_m::interrupt::free(|cs| {
        *TICK.borrow(cs).borrow_mut() += 1;
    });
}
