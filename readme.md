# STM32newboard

Rust firmware for stm32coolboard/newboard.
This is a very basic application:

 * Enables the USB CDC serial port on board
 * Read analog signal from AN_BATTERY pin every 5 seconds.
 * One may enable 12v and 5v FET using USB serial
 * Toggle the led via USB serial 


This software is using https://crates.io/crates/stm32f0xx-hal and https://crates.io/crates/stm32-usbd.

Thanks to all the developers of the Rust embedded HAL framework!

It took me around 4 hours to setup this and it was way easier than using the horrible C HAL API from ST Microsystem.
